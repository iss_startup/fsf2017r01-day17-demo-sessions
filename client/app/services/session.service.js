(function(){
    angular
        .module("ShoppingCartApp")
        .service("sessionService", sessionService);

    sessionService.$inject = ["$http", "$q"];

    function sessionService($http, $q) {

        var vm = this;

        // Exposed functions
        vm.viewCart = viewCart;
        vm.addToCart = add;
        vm.checkout = _delete;

        // Function definition
        function add(newItem) {
            return $http.post("/api/cart", { item: newItem });
        }

        function _delete() {
            return $http.delete("/api/cart");
        }

        function viewCart() {
            return $http.get("/api/cart");
        }
    }
})();
