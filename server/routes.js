'use strict';
var express = require("express");
var path = require("path");          // Node module for working with files and directories
const CLIENT_FOLDER = path.join(__dirname + '/../client');

// Exposed functions
module.exports = {
    init: configureRoutes,                   
    errorHandler: errorHandler
}

// Function definition
function configureRoutes(app) {
    app.get("/api/cart", function (req, res) {
        res.status(200).json(req.session.cart);
    });

    app.post("/api/cart", function (req, res) {
        console.log("Add");
        req.session.cart.push(req.body.item);
        res.status(202).end();

    })

    app.delete("/api/cart", function (req, res) {
        //Save cart to database first before destroying
        req.session.destroy();
        res.status(200).end();
    });

    app.use(express.static(CLIENT_FOLDER, {
        index: "shoppingcart.html"
    }));
};

function errorHandler(app) {
    app.use(function (req, res) {
        res.status(401).sendFile(CLIENT_FOLDER + "/app/errors/404.html");
    });

    app.use(function (err, req, res, next) {
        res.status(500).sendFile(path.join(CLIENT_FOLDER + '/app/errors/500.html'));
    });
};